#!/bin/bash
# Sync installation things # now only installs additional
progs_file="./to_install.csv"
name=$USER


aurhelper="yay"

### FUNCTIONS ###

error() { clear; printf "ERROR:\\n%s\\n" "$1"; exit;}

[ -f $progs_file ] || error "Please generate $progs_file"


maininstall() { # Installs all needed programs from main repo.
    echo "installing main $1"
	sudo pacman --noconfirm --needed -S "$1" >/dev/null 2>&1
	}

gitmakeinstall() {
	dir=$(mktemp -d)
	dialog --title "LARBS Installation" --infobox "Installing \`$(basename "$1")\` ($n of $total) via \`git\` and \`make\`. $(basename "$1") $2" 5 70
	git clone --depth 1 "$1" "$dir" >/dev/null 2>&1
	cd "$dir" || exit
	make >/dev/null 2>&1
	make install >/dev/null 2>&1
	cd /tmp || return ;}

aurinstall() { # TODO - doesnt work
  echo "installing aur $1"
	echo "$aurinstalled" | grep "^$1$" >/dev/null 2>&1 && return
	$aurhelper -S --noconfirm "$1"
	}

pipinstall() { \
	dialog --title "LARBS Installation" --infobox "Installing the Python package \`$1\` ($n of $total). $1 $2" 5 70
	command -v pip || sudo pacman -S --noconfirm --needed python-pip >/dev/null 2>&1
	yes | pip install "$1"
	}

installationloop() { \
	total=$(wc -l < $progs_file) 
	aurinstalled=$(sudo pacman -Qm | awk '{print $1}')
	while IFS=, read -r tag program comment; do
		n=$((n+1))
		echo "$comment" | grep "^\".*\"$" >/dev/null 2>&1 && comment="$(echo "$comment" | sed "s/\(^\"\|\"$\)//g")"
		case "$tag" in
			"A") aurinstall "$program" "$comment" ;;
			"G") gitmakeinstall "$program" "$comment" ;;
			"P") pipinstall "$program" "$comment" ;;
			"") maininstall "$program" "$comment" ;;
		esac
	done < $progs_file ;}

refreshkeys() { \
	sudo pacman --noconfirm -Sy archlinux-keyring >/dev/null 2>&1
}

refreshkeys || error "Error automatically refreshing Arch keyring. Consider doing so manually."
installationloop
